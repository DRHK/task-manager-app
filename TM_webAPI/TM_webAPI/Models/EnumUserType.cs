﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM_webAPI.Models
{
    public enum EnumUserType
    {
        None = 0,
        Employer = 1,
        Employee = 2
    }
}