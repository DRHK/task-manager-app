﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM_webAPI.Models
{
    public class TaskDetails
    {
        public string TaskName { get; set; }
        public List<TaskItem> TaskItems { get; set; }

    }
}