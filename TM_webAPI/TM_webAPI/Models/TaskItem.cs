﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM_webAPI.Models
{
    public class TaskItem
    {
        //sets the task items
        public int ID { get; set; }
        public string Name { get; set; }
        public string Req { get; set; }
        public string DueDate { get; set; }
        public int Amount { get; set; }

    }
}