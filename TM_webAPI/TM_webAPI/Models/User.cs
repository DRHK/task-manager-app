﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM_webAPI.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public int Id { get; set; }
        public EnumUserType UserType { get; set; }
    }
}