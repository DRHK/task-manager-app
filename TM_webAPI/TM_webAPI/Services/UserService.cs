﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM_webAPI.Models;

namespace TM_webAPI.Services
{
    public static class UserService
    {
        //the list of usernames and passwords (only david and dora are existing others are created for binary search and sort to show functionality)
        public static List<User> Users = new List<User>()
        {
            new User
            {
                UserName = "david",
                Password = "123",
                Id = 1,
                UserType = EnumUserType.Employee
            },
            new User
            {
                UserName = "jack",
                Password = "123",
                Id = 3,
                UserType = EnumUserType.Employee
            },
            new User
            {
                UserName = "ben",
                Password = "123",
                Id = 2,
                UserType = EnumUserType.Employee
            },
            new User
            {
                UserName = "connor",
                Password = "123",
                Id = 5,
                UserType = EnumUserType.Employee
            },
            new User
            {
                UserName = "oly",
                Password = "123",
                Id = 9,
                UserType = EnumUserType.Employee
            },
            new User
            {
                UserName = "dora",
                Password = "321",
                Id = 4,
                UserType = EnumUserType.Employer
            },
            new User
            {
                UserName = "brad",
                Password = "321",
                Id = 8,
                UserType = EnumUserType.Employer
            },
            new User
            {
                UserName = "george",
                Password = "321",
                Id = 6,
                UserType = EnumUserType.Employer
            },
            new User
            {
                UserName = "nathan",
                Password = "321",
                Id = 7,
                UserType = EnumUserType.Employer
            }
        };

        //Function that uses the data from the binary search to validate if the entered details are correct
        public static User LoginCheck(string username, string password)
        {
            var user = SearchUserByName(Users, username);

            if(user != null && user.Password == password)
            {
                return user;
            }

            return null;            
        }
        //Binary search for user 
        public static User SearchUserByName(List<User> list, string username)
        {
            var sortedList = OrderUserByName(list);

            Int32 startSearch = 0;
            Int32 endSearch = sortedList.Count - 1;

            while (startSearch <= endSearch)
            {
                var middle = (startSearch + endSearch) / 2; 
                var currentItem = sortedList[middle];

                if (currentItem.UserName == username) //checks if search key is the same as the saved username
                {
                    return currentItem;
                }
                else
                {
                    var myOutput = string.Compare(currentItem.UserName, username); // compares the searchkey to the the current item in the list and decides position to set new middle

                    if (myOutput > 0)
                    {
                        endSearch= middle - 1;
                    }
                    else
                    {
                        startSearch = middle + 1;
                    }
                }
            }

            return null;            
        }
        //Sorts the list of users by their username
        public static List<User> OrderUserByName(List<User> list)
        {
            int listlength;

            listlength = list.Count;
            var loopTo = listlength - 2;
            for (var j = 0; j <= loopTo; j++)
            {
                var loopTo1 = listlength - 1;
                for (var i = j + 1; i <= loopTo1; i++)
                {
                    var myOutput = string.Compare(list[j].UserName, list[i].UserName);
                    if (myOutput > 0)
                    {
                        // swaps values of list
                        var tmp = list[j];
                        list[j] = list[i];
                        list[i] = tmp;
                    }
                }
            }

            return list;
        }
    }
}