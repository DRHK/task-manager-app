﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM_webAPI.Services
{
    public static class TaskRepository
    {
        //repoistory for all task rows
        public static List<Models.TaskItem> TaskItems = new List<Models.TaskItem>();
        public static void Init()
        {
            TaskItems = new List<Models.TaskItem>();
            TaskItems.Add(new Models.TaskItem()
            {
                ID = 1,
                Name = "Do Dishes",
                Req = "Clean Dishes and put away",
                DueDate = "ASAP",
                Amount = 5
            });

            TaskItems.Add(new Models.TaskItem()
            {
                ID = 2,
                Name = "Wash Clothes",
                Req = "Wash dirty clothes and dry",
                DueDate = "Tomorrow",
                Amount = 8
            });

            TaskItems.Add(new Models.TaskItem()
            {
                ID = 3,
                Name = "Make dinner",
                Req = "Make everyone dinner",
                DueDate = "tonight by 6",
                Amount = 20
            });

            TaskItems.Add(new Models.TaskItem()
            {
                ID = 4,
                Name = "Clean house",
                Req = "Vaccum and mop floors",
                DueDate = "by 16/07/2019",
                Amount = 10
            });
        }
    }
}