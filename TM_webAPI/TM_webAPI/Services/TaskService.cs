﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM_webAPI.Models;

namespace TM_webAPI.Services
{

    public class TaskService
    {
        /// <summary>
        /// Binary search to search task by ID
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="taskId"></param>
        /// <returns>null or current item</returns>
        public static TaskItem SearchTaskByIdBinary(List<TaskItem> listName, int taskId)
        {
            var sortedList = OrderByTaskID(listName); //calls the sorted list

            Int32 startSearch = 0;
            Int32 endSearch = TaskRepository.TaskItems.Count - 1; //sets the end point of the binary search

            while (startSearch <= endSearch)
            {
                var middle = (startSearch + endSearch) / 2;
                var currentItem = TaskRepository.TaskItems[middle]; //sets the middle

                if (currentItem.ID == taskId)
                {
                    return currentItem;
                }
                else
                {
                    // changes the start or middle point depending on where the searchkey is(left or right of current item)
                    if (currentItem.ID > taskId){ 
                        endSearch = middle - 1;
                    }
                    else
                    {
                        startSearch = middle + 1;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Binary search to search through tasks by name
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="taskId"></param>
        /// <returns>null or current item</returns>
        public static TaskItem SearchTaskByNameBinary(List<TaskItem> listName, string taskName)
        {
            var sortedList = OrderByTaskName(listName);

            Int32 startSearch = 0;
            Int32 endSearch = TaskRepository.TaskItems.Count - 1;

            while (startSearch <= endSearch)
            {
                var middle = (startSearch + endSearch) / 2;
                var currentItem = TaskRepository.TaskItems[middle];

                if (currentItem.Name.ToLower().Contains(taskName.ToLower()))//Check if the list item contains the searchkey, also converts both to lowercase for case sensitive search
                {
                    return currentItem;
                }
                else
                {
                    var myOutput = string.Compare(currentItem.Name, taskName); // compares the searchkey to the the current item in the list and decides position to set new middle
                    // changes the start or middle point depending on where the searchkey is(left or right of current item)
                    if (myOutput > 0)
                    {
                        endSearch = middle - 1;
                    }
                    else
                    {
                        startSearch = middle + 1;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Sorts the Task Item list by ID
        /// </summary>
        /// <param name="listName"></param>
        /// <returns></returns>
        public static List<Models.TaskItem> OrderByTaskID(List<TaskItem> listName)
        {
            int taskListLength;

            taskListLength = TaskRepository.TaskItems.Count; //set the length to the length of the list 
            var loopTo = taskListLength - 2; // sets loop length to 2 less then the length of list
            for (var j = 0; j<= loopTo; j++) 
            {
                var loopTo1 = taskListLength - 1; // sets loop to the length minus 1
                for (var i = j + 1; i <= loopTo1; i++)
                {                    
                    // if the item in position "j" is greater than the one in position "i" then the valuse get swapped
                    if(TaskRepository.TaskItems[j].ID > TaskRepository.TaskItems[i].ID) 
                    {
                        //swaps the values of "j" and "i"
                        var tmp = TaskRepository.TaskItems[j];
                        TaskRepository.TaskItems[j] = TaskRepository.TaskItems[i];
                        TaskRepository.TaskItems[i] = tmp;
                    }
                }
            }
            return TaskRepository.TaskItems;
        }

        public static List<Models.TaskItem> OrderByTaskName(List<TaskItem> listName)
        {
            int taskListLength;

            taskListLength = TaskRepository.TaskItems.Count; //set the length to the length of the list 
            var loopTo = taskListLength - 2; // sets loop length to 2 less then the length of list
            for (var j = 0; j <= loopTo; j++)
            {
                var loopTo1 = taskListLength - 1; // sets loop to the length minus 1
                for (var i = j + 1; i <= loopTo1; i++)
                {
                    // if the item in position "j" is greater than the one in position "i" then the valuse get swapped
                    var myOutput = string.Compare(listName[j].Name, listName[i].Name);
                    if (myOutput > 0)
                    {
                        //swaps the values of "j" and "i"
                        var tmp = listName[j];
                        listName[j] = listName[i];
                        listName[i] = tmp;
                    }
                }
            }
            return listName;
        }

    }
}