﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TM_webAPI.Services;

namespace TM_webAPI.Controllers
{
    public class TaskController : ApiController
    {
        

        // GET: api/Task
        public Models.TaskDetails Get()
        {
            return new Models.TaskDetails() {
                TaskItems = TaskRepository.TaskItems,
                TaskName = "House Chores"             
            };
        }

       // Save - Create new / Edit
       [HttpGet, Route("api/task/save/")]        
        public string Save(int id, string name, string req, string dueDate, int amount)
        {
            
            var task = new Models.TaskItem()
            {
                ID = id,
                Name = name,
                Req = req,
                Amount = amount,
                DueDate = dueDate
            };

            var newTaskId = TaskRepository.TaskItems.Max(x => x.ID) + 1;
            if (task.ID == 0) //if the task has no id then a new task will be created else the changes will be updated on the row
            {
                // Generate new ID - max ID + 1
                
                task.ID = newTaskId;

                // Insert new task intop the task list
                TaskRepository.TaskItems.Add(task);                
            }
            else
            {
                // Update existing
                var taskToEdit = TaskService.SearchTaskByIdBinary(TaskRepository.TaskItems, id); //passes id through to the function SearchTaskByIdBinary whats a binary search for the tasks by id
                if (taskToEdit != null) // if the search is not null then will edit the changes
                {
                    taskToEdit.Name = task.Name;
                    taskToEdit.Req = task.Req;
                    taskToEdit.DueDate = task.DueDate;
                    taskToEdit.Amount = task.Amount;
                };
            }

            return newTaskId.ToString();
        }

        // Delete
        [HttpGet, Route("api/task/delete")]        
        public string Delete(int id)
        {
             var taskToDelete = TaskService.SearchTaskByIdBinary(TaskRepository.TaskItems, id); ///passes id through to the function SearchTaskByIdBinary whats a binary search for the tasks by id 
            TaskRepository.TaskItems.Remove(taskToDelete); //If the task was found then it will be returned
            return "ok";
        }

        // Search Task
        [HttpGet, Route("api/task/search")]
        public List<Models.TaskItem> Search(string name)
        {
            var taskToSearch = TaskService.SearchTaskByNameBinary(TaskRepository.TaskItems, name); //passes name to the binary search that searches for the tasks name
            if (taskToSearch != null)
            {
                return new List<Models.TaskItem>()
                {
                    taskToSearch //returns the row that was searched for
                };
            }
            else
            {
                return null;
            }
        }
    }
}
