﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TM_webAPI.Models;
using TM_webAPI.Services;

namespace TM_webAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {        
        public object Get(string username, string password)
        {

             

            var user = UserService.LoginCheck(username, password); //Calls the service called UserService and the function Login check and passes through the data of (username, password)

            // Success check
            var userType = EnumUserType.None;
            var success = false;

            if(user == null)
            {
                success = false;
            }
            else
            {
                success = true;
                userType = user.UserType;
               
            }

                return new {
                success,
                userType
            
            };
        }
    }    
};
    