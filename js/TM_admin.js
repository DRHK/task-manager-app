var TaskItems = [];
var EditedRowID = 0;
var DeleteRowID = 0;
var btnEditShow = function(i) {
    $('#editModal').modal('show');
    // adds the values of the row into the edit modal
    var row = TaskItems[i];
    $("#rowName").val(row.Name);
    $("#rowReq").val(row.Req);
    $("#rowDue").val(row.DueDate);
    $("#rowAmount").val(row.Amount);
    EditedRowID = row.ID
    //send changed table to API here
    
};

var buildGrid = function (list) {
    //generates new rows for each list item recieved from the webAPI
    for (var i = 0; i < list.length; i++) { // goes through the all the TaskItems in the list
            var dataRow = list[i]
        var row = document.createElement('tr'); // create row node
        var col0 = document.createElement('td');
            var col = document.createElement('td'); // create column node
            var col2 = document.createElement('td'); // create second column node
            var col3 = document.createElement('td');
            var col4 = document.createElement('td');
            var colAction = document.createElement('td');
        row.appendChild(col0); // append first column to row
        row.appendChild(col);
            row.appendChild(col2); // append second column to row
            row.appendChild(col3);
            row.appendChild(col4);
            row.appendChild(colAction);
            col.innerHTML = dataRow.Name;; // put data in first column
            col2.innerHTML = dataRow.Req; // put data in second column
            col3.innerHTML = dataRow.DueDate;
            col4.innerHTML = dataRow.Amount;
            colAction.innerHTML = "<button id='btnEdit' onclick= 'btnEditShow(" + i + ")' type='button' rel='tooltip' class='btn btn-success btn-simple'> <i class='material-icons'>edit</i></button> <button id='btnDelete' onclick= 'btnDeleteShow(" + i +")' type='button' rel='tooltip' class='btn btn-danger btn-simple'> <i class='material-icons'>close</i> </button>";
            var table = document.getElementById("tableRow"); // find table to append to
            table.appendChild(row); // append row to table
            
        } 
}


var btnDeleteShow = function(i) {
    $('#deleteModal').modal('show');
    var row = TaskItems[i];
    DeleteRowID = row.ID
    //send api deleted row here
    $("#btnDelete1").off("click").click(function () { 
        $.ajax({
            contentType: 'application/json',
            type: "get",
           url: "http://localhost/TM.API/api/Task/delete/",
            data: {
                id: DeleteRowID
            },
            success: function (data, textStatus, jqXHR) {
                console.log(data)
                document.getElementById("tableRow").deleteRow(i);//deletes row by index
                location.reload();
            }
        });

        $('#deleteModal').modal('hide');
        document.getElementById("tableRow").deleteRow(i);
    });

};

var AddNewRow = function (i) {
    $('#rowModal').modal('hide');   //send changed table to API here
    var row = document.createElement('tr'); // create row node
    var col0 = document.createElement('td');
    var col = document.createElement('td'); // create column node
    var col2 = document.createElement('td'); // create second column node
    var col3 = document.createElement('td');
    var col4 = document.createElement('td');
    var colAction = document.createElement('td');
    var newRowName = document.getElementById("newRowName").value;
    var newRowReq = document.getElementById("newRowReq").value;
    var newRowDue = document.getElementById("newRowDue").value;
    var newRowAmount = document.getElementById("newRowAmount").value;
    row.appendChild(col0); 
    row.appendChild(col);// append first column to row
    row.appendChild(col2); // append second column to row
    row.appendChild(col3);
    row.appendChild(col4);
    row.appendChild(colAction);
    col.innerHTML = newRowName; // put data in first column
    col2.innerHTML = newRowReq; // put data in second column
    col3.innerHTML = newRowDue;
    col4.innerHTML = newRowAmount;
    // code adds a edit and delete button to a table
    colAction.innerHTML = "<button id='btnEdit' onclick= 'btnEditShow(" + i + ")' type='button' rel='tooltip' class='btn btn-success btn-simple'> <i class='material-icons'>edit</i></button> <button id='btnDelete' onclick= 'btnDeleteShow(" + i + ")' type='button' rel='tooltip' class='btn btn-danger btn-simple'> <i class='material-icons'>close</i> </button>";
    var table = document.getElementById("tableRow"); // find table to append to
}

$(document).ready(function () {
    $("#newRow").click(function show() { //funtion gets called by the id at the start
        $('#rowModal').modal('show');// shows the modal pop up
    });
    $("#search").click(function searchTask() {
        var searchKey = $("#searchKey").val();
        if (searchKey !== undefined && searchKey !== null && searchKey.length > 0) { // checks to see if the searchbox is empty or not and if empty shows error message
            $.ajax({
                dataType: 'json',
                type: "get",
                url: "http://localhost/TM.API/api/Task/Search/", // link for where the api is
                data: {
                    name: searchKey

                },
                success: function (data, textStatus, jqXHR) {
                    if (data !== null) {
                        $("#tableRow tr").remove();
                        buildGrid(data);
                    } else {
                        $('#errorModal').modal('show');
                    }
                }
            });
        } else {
            $('#enterTextModal').modal('show'); //displays error message
        }
    });

    $('#saveNewRow').click(function save() {
        var Amount = $("#newRowAmount").val();
        if (Amount !== undefined && searchKey !== null && Amount.length > 0) { // checks to see if the amount box is empty or not and if empty shows error message
            $.ajax({
                dataType: 'json',
                type: "get",
                url: "http://localhost/TM.API/api/Task/Save/",
                data: {

                    id: 0,
                    name: $("#newRowName").val(),
                    req: $("#newRowReq").val(),
                    dueDate: $("#newRowDue").val(),
                    amount: $("#newRowAmount").val()
                },
                success: function (data, textStatus, jqXHR) {
                    //   AddNewRow(data.ID) 
                    location.reload();
                    $('#saveModal').modal('show');

                }
            });
        } else {
            $('#rowModal').modal('hide');
            $('#enterTextModal').modal('show');            //displays error message
        }

       
        


    });
    
    $('#saveRow').click(function save() {

        $('#editModal').modal('hide');   //send changed table to API here

        var Amount = $("#rowAmount").val();
        if (Amount !== undefined && searchKey !== null && Amount.length > 0) {// checks to see if the amount box is empty or not and if empty shows error message
            $.ajax({
                dataType: 'json',
                type: "get",
                url: "http://localhost/TM.API/api/Task/Save/",
                data: {

                    id: EditedRowID,
                    name: $("#rowName").val(),
                    req: $("#rowReq").val(),
                    dueDate: $("#rowDue").val(),
                    amount: $("#rowAmount").val()
                },
                success: function (data, textStatus, jqXHR) {
                    $('#saveModal').modal('show');
                }
            });
            location.reload();
        } else {
            $('#editModal').modal('hide');
            $('#enterTextModal').modal('show');//displays error message
        }

        
    });
    
    

});

//When page first loads the table is built

$.ajax({
    contentType: 'application/json',
    type: "Get",
    url: "http://localhost/TM.API/api/Task",
    data: {
        
    },
    success: function (data, textStatus, jqXHR) {
        TaskItems = data.TaskItems;
        
        buildGrid(data.TaskItems);
    },
    error: function (jqXHR, textStatus, errorThrown) {
        $("#postResult").val(jqXHR.statusText);
    }

});

    