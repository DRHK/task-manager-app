// JavaScript source code
$("#btnLogin").click(function () {

    
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var Employer = false;
    var Employee = false;

    var loginCheck = function(data) {
        if (data.success == false) { //checks if the username and password match database
            $('#exampleModal').modal('show'); // if not then shows error message
        } else if(data.userType == 1) { // if the user is an admin is taken to admin page
            window.location.href = "http://localhost/TM/adminHome.html";
        } else if (data.userType == 2) {// else if the user is not an admin is taken to user page
            window.location.href = "http://localhost/TM/userHome.html";
            
        }

    } 


    $.ajax({
        contentType: 'application/json',
        type: "Get",
        url: "http://localhost/TM.API/api/login",
        data: {
            username: document.getElementById('username').value, // send username to webAPI
            password: document.getElementById('password').value // send password entered to webAPI
        },
        success: function (data, textStatus, jqXHR) {
            loginCheck(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#postResult").val(jqXHR.statusText);
        }

    });

});
