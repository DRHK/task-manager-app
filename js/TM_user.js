$(document).ready(function () {
    
    $("#search1").click(function searchTask() {
        var searchKey = $("#searchKey1").val();
        if (searchKey !== undefined && searchKey !== null && searchKey.length > 0) { // checks to see if the searchbox is empty or not and if empty shows error message
            $.ajax({
                dataType: 'json',
                type: "get",
                url: "http://localhost/TM.API/api/Task/Search/", // link for where the api is
                data: {
                    name: searchKey

                },
                success: function (data, textStatus, jqXHR) {
                    if (data !== null) {
                        $("#tableRow tr").remove();
                        buildGrid(data);
                    } else {
                        $('#errorModal').modal('show');
                    }
                }
            });
        } else {
            $('#enterTextModal').modal('show'); //displays error message
        }
    });
});
var buildGrid = function (list) {
    //generates new rows for each list item recieved from the webAPI
    for (var i = 0; i < list.length; i++) { // goes through the all the TaskItems in the list
        var dataRow = list[i]
        var row = document.createElement('tr'); // create row node
        var col0 = document.createElement('td');
        var col = document.createElement('td'); // create column node
        var col2 = document.createElement('td'); // create second column node
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var colAction = document.createElement('td');
        row.appendChild(col0); // append first column to row
        row.appendChild(col);
        row.appendChild(col2); // append second column to row
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(colAction);
        col.innerHTML = dataRow.Name;; // put data in first column
        col2.innerHTML = dataRow.Req; // put data in second column
        col3.innerHTML = dataRow.DueDate;
        col4.innerHTML = dataRow.Amount;
        var table = document.getElementById("tableRow"); // find table to append to
        table.appendChild(row); // append row to table

    }
}
$.ajax({
    contentType: 'application/json',
    type: "Get", // what is being done
    url: "http://localhost/TM.API/api/Task", //the url of the api
    data: {

    },
    success: function (data, textStatus, jqXHR) {
        TaskItems = data.TaskItems;
        buildGrid(data.TaskItems);

    },
    error: function (jqXHR, textStatus, errorThrown) {
        $("#postResult").val(jqXHR.statusText);
    }
});
